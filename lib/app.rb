require 'sinatra'
require 'mongo'
require 'json'

include Mongo

get '/' do
  ENV.to_a.to_s
end

put '/:key' do
  data = params[:data]
  if data
    mongodb_collection.insert(
      key: params[:key],
      data: data
                              )
    status 201
    body 'success'
  else
    status 400
    body 'data field missing'
  end
end

get '/:key' do
  doc = mongodb_collection.find_one(key: params[:key])
  if doc
    status 200
    body doc.fetch('data')
  else
    status 404
    body 'key not present'
  end
end

def mongodb_credentials
  JSON.parse(ENV.fetch('VCAP_SERVICES')).fetch('mongodb-2.2').first.fetch('credentials')
end

def mongodb_collection
  credentials = mongodb_credentials

  host = credentials.fetch('host')
  port = credentials.fetch('port')
  username = credentials.fetch('username')
  password = credentials.fetch('password')
  db = credentials.fetch('db')

  client = MongoClient.new(host, port)
  db = client.db(db)
  auth = db.authenticate(username, password)
  fail "Unable to authorize against db: #{credentials.inspect}" unless auth

  db.collection('test')
end
